## EGUI

```
USER coder
WORKDIR /home/coder
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN sudo apt-get update && sudo apt-get install -y --no-install-recommends bash
ENV PATH="/home/coder/.cargo/bin:${PATH}"
RUN sudo apt-get install -y libclang-dev libgtk-3-dev libxcb-render0-dev libxcb-shape0-dev libxcb-xfixes0-dev libxkbcommon-dev libssl-dev
RUN git clone https://github.com/emilk/egui.git
RUN cd egui && ls -l && cd examples/hello_world_simple && cargo build
```

### Only Rust Dev
```bash
# Use non-root user
USER coder
WORKDIR /home/coder

#update Python
RUN sudo apt update
RUN sudo apt install -y python3.7 python3-pip
RUN sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
RUN sudo apt install -y python3.7-dev libssl-dev

# Install Rust 
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"

RUN git clone https://gitlab.com/noahgift/rust-data-engineering.git \ 
    && cd rust-data-engineering && ./build.sh     && cd ..
```

### Rust Cargo Lambda

Steps to recreate:

```bash
cargo lambda watch -a 127.0.0.1 -p 5050
cargo lambda invoke -a 127.0.0.1 -p 5050 --data-ascii "{ \"greeting\": \"Marco\"}"

```

```bash
# Use non-root user
USER coder
WORKDIR /home/coder

## Remove Eclipse
# Remove Eclipse download and extract
RUN sudo rm -rf /opt/eclipse
# Remove Gradle
RUN sudo rm -rf /opt/gradle

#Remove large files in /usr/lib
RUN sudo rm -rf /usr/lib/jvm
RUN sudo rm -rf /usr/lib/firefox
RUN sudo rm -rf /usr/lib/jvm

## Remove Wireshark
RUN sudo apt purge -y wireshark-common wireshark-qt

#Homebrew
ENV NONINTERACTIVE=1
ENV HOMEBREW_PREFIX=/home/linuxbrew/.linuxbrew
RUN curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh | bash
ENV PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"
RUN brew tap cargo-lambda/cargo-lambda
RUN brew install cargo-lambda


# Install Rust 
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"

# Build AWS Lambda
RUN git clone https://gitlab.com/noahgift/aws-lambda-rust.git     && cd aws-lambda-rust && ./build.sh     && cd ..
```


### PYO3

Steps to recreate:

```bash
python3.7 -m pip install virtualenv
which virtualenv
virtualenv ~/.venv
source ~/.venv/bin/activate
cd rust-with-python3/pycalc-cli
make build
which python
pip install fire
python calc.py add 2 2
```


```bash
# Use non-root user
USER coder
WORKDIR /home/coder

## Remove Eclipse
# Remove Eclipse download and extract
RUN sudo rm -rf /opt/eclipse
# Remove Gradle
RUN sudo rm -rf /opt/gradle

#Remove large files in /usr/lib
RUN sudo rm -rf /usr/lib/jvm
RUN sudo rm -rf /usr/lib/firefox


# Remove Eclipse workspace and config dirs
RUN sudo rm -rf ${ECLIPSE_WORKSPACE} ${HOME}/.eclipse
# Remove Eclipse launcher
RUN sudo rm -f ${HOME}/Desktop/Eclipse.desktop

## Remove Wireshark
RUN sudo apt purge -y wireshark-common wireshark-qt

# Install Python3.7 and remove 3.6
#update Python
RUN sudo apt update
RUN sudo apt remove -y python3.6 
RUN sudo apt autoremove -y
RUN sudo apt install -y python3.7 python3-pip python3.7-dev python3-venv
RUN sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1

# Install Rust 
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"

# Build Rust with Python
RUN git clone https://gitlab.com/noahgift/rust-with-python.git     && cd rust-with-python && ./build.sh 
```



## LABS (too big)
```bash
# Use non-root user
USER coder
WORKDIR /home/coder

#update Python
RUN sudo apt update
RUN sudo apt install -y python3.7 python3-pip
RUN sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
RUN sudo apt install -y python3.7-dev

# Install Rust 
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"

RUN sudo apt-get install -y libclang-dev libgtk-3-dev libxcb-render0-dev libxcb-shape0-dev libxcb-xfixes0-dev libxkbcommon-dev libssl-dev

RUN curl -fL https://github.com/lapce/lapce/releases/latest/download/Lapce-linux.tar.gz -o lapce.tar.gz   && tar -xzf lapce.tar.gz \
  && mv Lapce lapce \
  && rm lapce.tar.gz

# Clone example GUI repo and build
RUN git clone https://github.com/emilk/egui.git \
   && cd egui/examples/hello_world_simple \
   && cargo build


# Clone lab repos
RUN git clone https://gitlab.com/dukeaiml/llmops-aws.git \
    && cd llmops-aws && ./build.sh \
    && cd ..

RUN git clone https://gitlab.com/noahgift/rust-with-python.git \
    && cd rust-with-python && ./build.sh \ 
    && cd ..
    
RUN git clone https://gitlab.com/noahgift/aws-lambda-rust.git \
    && cd aws-lambda-rust && ./build.sh \
    && cd ..

RUN git clone https://gitlab.com/noahgift/rust-data-engineering.git \ 
    && cd rust-data-engineering && ./build.sh \
    && cd ..
```

### Remove Eclipse and Wireshark

```bash
# Use non-root user
USER coder
WORKDIR /home/coder

## Remove Eclipse
# Remove Eclipse download and extract
RUN sudo rm -rf /opt/eclipse
# Remove Eclipse workspace and config dirs
RUN sudo rm -rf ${ECLIPSE_WORKSPACE} ${HOME}/.eclipse
# Remove Eclipse launcher
RUN sudo rm -f ${HOME}/Desktop/Eclipse.desktop

# Remove Firefox configs
RUN sudo rm -rf /etc/firefox /tmp/firefox-launch-page
# Remove Eclipse
RUN sudo apt purge -y eclipse-platform \
eclipse-jee                   eclipse-rcp

## Remove Wireshark
RUN sudo apt purge -y wireshark-common wireshark-qt
RUN sudo rm ${HOME}/Desktop/Wireshark.desktop
RUN sudo rm -rf /usr/share/applications/wireshark.desktop
RUN sudo rm /usr/bin/wireshark
RUN sudo rm /etc/apt/sources.list.d/wireshark*
RUN sudo groupdel wireshark
```

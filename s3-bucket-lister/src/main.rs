use std::boxed::Box;

async fn list_buckets() -> Result<(), Box<dyn std::error::Error>> {
    let config = aws_config::load_from_env().await;
    let client = aws_sdk_s3::Client::new(&config);
    let resp = client.list_buckets().send().await?;
    let resp_buckets = resp.buckets.unwrap_or_default();
    let num_buckets = resp_buckets.len();
    println!("Found {} buckets:", num_buckets);
    for bucket in resp_buckets {
        println!("  {}", bucket.name.unwrap_or_default());
    }
    Ok(())
}

#[::tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    list_buckets().await?;
    Ok(())
}

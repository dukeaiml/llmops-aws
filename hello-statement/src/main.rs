// This is a single line comment, and is ignored by the compiler.
/*
This is a multiline comment.
*/

fn main() {

// Print text with formatting to the console.
    let name = "Ada";
    let number = 45;
    println!("Hello {name}! The number is {number}");
}
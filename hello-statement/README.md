**Quick Summary**

This simple program prints a formatted greeting with a name and number to the console. It shows basic concepts like variables, strings, and println macro.

**Lab Instructions**

- Examine how `name` and `number` variables are declared with `let`
- Note string formatting syntax in `println!` macro
- Try modifying the name and number values
- Add additional placeholders like `{number * 2}`
- Run the code to view the output
- Refer to errors to debug issues

**Challenge Exercises**

- Prompt user to input name and number
- Print greeting in Title Case format
- Handle errors using `std::error` instead of default